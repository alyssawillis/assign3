var productNumber = ["p100", "p200", "p300", "p400", "p500", "p600"];
var description = ["apple", "banana", "orange", "kiwi", "peach", "pear"];
var quantity = [8, 6, 3, 1, 4, 7];
var price = [20, 10, 30, 10, 25, 15];
var fee = [5, 0, 0, 5, 1, 3];

var products = [];
products.push(productNumber);
products.push(description);
products.push(quantity);
products.push(price);
products.push(fee);

var pictures = ["again.jpg", "air.jpg", "ball.jpg", "blue.jpg", 
                "branch.jpg", "capsule.jpg", "cesta.jpg", "forest.jpg", 
                "golf.jpg", "grass.jpg", "life.jpg", "never.jpg", 
                "one.jpg", "paper.jpg", "park.jpg", "path.jpg", 
                "road.jpg", "secret.jpg", "semi.jpg", "star.jpg", 
                "trees.jpg", "two.jpg", "winter.jpg", "zivot.jpg"];

function getShuffledUniqueImageNames(pictures) {
    var pictures = pictures.slice(0);
    for (var index = 0; index < pictures.length; index++) {
        var randomize = Math.floor(Math.random() * pictures.length);
        var theValue = pictures[index];
        pictures[index] = pictures[randomize];
        pictures[randomize] = theValue;
    }
    return pictures;
}

function productionTableWithParameters(products, pictures) {
    var myTable = "";
    myTable = '<table border = "1">';
    var theValue = 0;
    
    for (var row = 1; row <= 6; row++) {
    var theValue = Math.floor(Math.random() * products.length);
    randPicture = pictures[theValue];
    var randPicture = getShuffledUniqueImageNames(products, pictures);
    myTable += '<tr>';
    myTable += '<td>' + theValue + '</td>';
    randPicture = theValue + pictures;
    if (randPicture > 50) {
        myTable += '<td bgcolor="#D3D3D3">' + randPicture + '</td>';
        } else {
        myTable += '<td>' + randPicture + '</td>';
    }
        myTable += '<tr>';
}
    var totalPictures = randPicture / 6;
    myTable += "<tr><td>" + theValue + "</td><td>" + randPicture + "</td></tr>";
    myTable += '</table>';
    return myTable;
}
    document.getElementById("part2").innerHTML = productionTableWithParameters(products, pictures);
    


